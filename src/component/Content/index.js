import clsx from "clsx";
import styles from './Content.module.scss'

function Content() {
    return (
        <div className={clsx('grid', 'wide')}>
            <div className={clsx('row')}>
                <div className={clsx('col', 'l-3', 'm-6', 'c-12')}>
                    <div className={clsx(styles.Content_Contry)}>
                        <div className={styles.ContentImg}></div>
                        <div className={clsx(styles.ContentContainer)}>
                            <h3>Vietnamese</h3>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div className={clsx('col', 'l-3', 'm-6', 'c-12')}>
                    <div className={clsx(styles.Content_Contry)}>
                        <div className={styles.ContentImg2}></div>
                        <div className={clsx(styles.ContentContainer)}>
                            <h3>Vietnamese</h3>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div className={clsx('col', 'l-3', 'm-6', 'c-12')}>
                    <div className={clsx(styles.Content_Contry)}>
                        <div className={styles.ContentImg3}></div>
                        <div className={clsx(styles.ContentContainer)}>
                            <h3>Vietnamese</h3>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div className={clsx('col', 'l-3', 'm-6', 'c-12')}>
                    <div className={clsx(styles.Content_Contry)}>
                        <div className={styles.ContentImg4}></div>
                        <div className={clsx(styles.ContentContainer)}>
                            <h3>Vietnamese</h3>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
            <div className="row">
                <div className={clsx('col', 'l-3', 'm-6', 'c-12')}>
                    <div className={clsx(styles.Content_Contry)}>
                        <div className={styles.ContentImg5}></div>
                        <div className={clsx(styles.ContentContainer)}>
                            <h3>Vietnamese</h3>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div className={clsx('col', 'l-3', 'm-6', 'c-12')}>
                    <div className={clsx(styles.Content_Contry)}>
                        <div className={styles.ContentImg6}></div>
                        <div className={clsx(styles.ContentContainer)}>
                            <h3>Vietnamese</h3>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div className={clsx('col', 'l-3', 'm-6', 'c-12')}>
                    <div className={clsx(styles.Content_Contry)}>
                        <div className={styles.ContentImg7}></div>
                        <div className={clsx(styles.ContentContainer)}>
                            <h3>Vietnamese</h3>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div className={clsx('col', 'l-3', 'm-6', 'c-12')}>
                    <div className={clsx(styles.Content_Contry)}>
                        <div className={styles.ContentImg8}></div>
                        <div className={clsx(styles.ContentContainer)}>
                            <h3>Vietnamese</h3>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                            <div className={clsx(styles.ContentImg_sub)}>
                                <h4 style={{ marginRight: "10px" }}>Population:</h4>
                                <p>90.000.889</p>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    )
}

export default Content;