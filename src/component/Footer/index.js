import clsx from "clsx";
import styles from './Footer.module.scss'
import { BsInstagram, BsFacebook, BsTwitter, BsYoutube, BsTelegram } from 'react-icons/bs'



function Footer() {
    return (
        <div className={clsx(styles.FooterContainer)}>
            <div className={clsx('grid', 'wide')}>
                <div className={clsx('row')}>
                    <div className={clsx('col', 'l-3', 'm-6', 'c-6', styles.FooterPhone)}>
                        <h5>Phone Support</h5>
                        <p>24 HOURS A DAY</p>
                        <h7 className={clsx(styles.FooterSub)}>+ 01 345 647 745</h7>
                    </div>

                    <div className={clsx('col', 'l-2', 'm-6', 'c-6', styles.FooterPhone)}>
                        <h5>Connect With Us</h5>
                        <p>SOCIAL MEDIA CHANNELS</p>
                        <div className={clsx(styles.FooterSocial)}>
                            <BsInstagram className={styles.Instagram} />
                            <BsFacebook className={styles.Facebook} />
                            <BsTwitter className={styles.Twitter}></BsTwitter>
                            <BsYoutube className={styles.Youtube}></BsYoutube>
                            <BsTelegram className={styles.Tele} />
                        </div>
                    </div>

                    <div className={clsx('col', 'l-3', 'm-6', 'c-6', styles.FooterPhone)}>
                        <h5>Phone Support</h5>
                        <p>24 HOURS A DAY</p>
                        <h7 className={clsx(styles.FooterSub)}>+ 01 345 647 745</h7>
                    </div>
                    <div className={clsx('col', 'l-4', 'm-6', 'c-6', styles.FooterPhone)}>
                        <h5>Newsletter</h5>
                        <p>SIGN UP FOR SPECIAL OFFERS</p>
                        <form action="">
                            <input required type="text" placeholder="email" className={clsx(styles.FooterInput)} />
                            <button type="submit" className={clsx(styles.FooterBtn)}>SUBSCRIBE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer;