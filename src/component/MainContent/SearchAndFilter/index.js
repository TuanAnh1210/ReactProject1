import React from "react";
import clsx from "clsx";
import { BsSearch, BsChevronDown } from 'react-icons/bs'
import { GiWorld } from 'react-icons/gi'
import styles from "./SearchAndFilter.module.scss";
function MainContent(props) {
    return (
        <div className={clsx('grid', 'wide')}>
            <div className={clsx('row', styles.Container)}>
                <div className={clsx('col l-5', styles.Search)}>
                    <h5>Search Country:</h5>
                    <div style={{ display: 'flex', boxShadow: '0 1px 10px #000' }}>
                        <input type="text" placeholder="Input the end enter to search..." />
                        <div className={clsx(styles.iconSearch)}>
                            <BsSearch className={clsx(styles.icon)} />
                        </div>
                    </div>
                </div>
                <div className={clsx('col l-o-2', styles.FilterZ)}>
                    <h5 style={{ fontSize: '20px', fontWeight: '600', margin: '16px 0 12px 0' }}>Filter By Regions</h5>
                    <div className={clsx(styles.FilterContainer)}>
                        <p>All</p>
                        <BsChevronDown className={clsx(styles.icon)}></BsChevronDown>
                        <div>
                            <ul>
                                <li>
                                    <GiWorld style={{ transform: 'translateY(50%)', margin: '0 8px 0 16px' }} />
                                    <p>All</p>
                                </li>
                                <li>
                                    <GiWorld style={{ transform: 'translateY(50%)', margin: '0 8px 0 16px' }} />
                                    <p>VietNam</p>
                                </li>
                                <li>
                                    <GiWorld style={{ transform: 'translateY(50%)', margin: '0 8px 0 16px' }} />
                                    <p>Itali</p>
                                </li>
                                <li>
                                    <GiWorld style={{ transform: 'translateY(50%)', margin: '0 8px 0 16px' }} />
                                    <p>French</p>
                                </li>
                                <li>
                                    <GiWorld style={{ transform: 'translateY(50%)', margin: '0 8px 0 16px' }} />
                                    <p>Thailand</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div className={clsx('row')} >
                <div style={{ display: 'flex', margin: '30px auto' }}>
                    <div className={clsx(styles.NumberContent, styles.active)}>
                        <span>1</span>
                    </div>
                    <div className={clsx(styles.NumberContent)}>
                        <span>2</span>
                    </div>
                    <div className={clsx(styles.NumberContent)}>
                        <span>3</span>
                    </div>
                    <div className={clsx(styles.NumberContent)}>
                        <span>4</span>
                    </div>
                    <div className={clsx(styles.NumberContent)}>
                        <span>5</span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default MainContent