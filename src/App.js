import Header from "./component/Header";
import './App.css'
import './grid.css'
import MainContent from "./component/MainContent/SearchAndFilter";
import Content from "./component/Content";
import Footer from "./component/Footer";
import FooterBottom from "./component/FooterBottom";

function App() {
  return (
    <div className="App">

      <Header />
      <MainContent />
      <Content />
      <Footer />
      <FooterBottom />


    </div>
  );
}

export default App;
