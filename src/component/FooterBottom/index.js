import styled from "styled-components"

function FooterBottom() {
    return (
        <Footerpane>
            <div>
                <p>© Copyright 2018 nicdark.com</p>
            </div>
            <div>
                <span>Best Travel WordPress Theme</span>
            </div>
        </Footerpane>
    )
}
export default FooterBottom;

const Footerpane = styled.div`
    display: flex;
    justify-content: space-between;
    padding : 18px 28px 18px 28px;

    p {
        font-size: 14px;
        color: #878787;
        letter-spacing: 2px;
    }
    
    span {
        font-size: 14px;
        color: #878787;
        letter-spacing: 2px;
    }
`
